package pragmatic.negative;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pragmatic.core.App;
import static org.testng.Assert.*;

public class RegisterTest {

    private App app;

    @BeforeMethod
    public void setup() {
        app = new App();
        app.startBrowser("http://shop.pragmatic.bg/");
    }

    @Test
    public void registerWithEmptyEmailTest() {
        app.homepage.gotoRegister();
        assertTrue(app.registerPage.isAccountDisplayed());
        app.registerPage.populateRegisterFieldsAndRegister("", "", "", "", "**", "");
        assertTrue(app.registerPage.isAlertDisplayed());
    }

    @AfterMethod
    public void tearDown() {
        app.quit();
    }
}
