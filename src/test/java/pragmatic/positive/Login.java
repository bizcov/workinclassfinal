package pragmatic.positive;

import net.bytebuddy.utility.RandomString;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pragmatic.core.App;
import static org.testng.Assert.assertTrue;

public class Login {

    private App app;
    private String email = "Ivan_Ivanov" + RandomString.make(8) + "@abv.bg";
    private String password = "petiletka";

    @BeforeMethod
    public void setup() {
        app = new App();
        app.startBrowser("http://shop.pragmatic.bg/");
        app.homepage.gotoRegister();
        app.registerPage.populateRegisterFieldsAndRegister("Ivan", "Ivanov", email, "0888888888", password, password);
        app.quit();
        app.startBrowser("http://shop.pragmatic.bg/");
    }

    @Test
    public void login() {
        app.homepage.gotoLogin();
        app.loginPage.login(email, password);
        assertTrue(app.successfulLoginPage.confirmLogin());
    }

    @AfterMethod
    public void tearDown() {
        app.quit();
    }
}
