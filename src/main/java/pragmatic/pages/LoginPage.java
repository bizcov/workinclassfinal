package pragmatic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pragmatic.pages.base.BasePage;

public class LoginPage extends BasePage {

    private static final By EMAIL_FIELD = By.id("input-email");
    private static final By PASSWORD_FIELD = By.id("input-password");
    private static final By LOGIN_BUTTON = By.cssSelector("input[value=Login]");
    private static final By UNSUCCESSFUL_LOGIN_ALERT = By.cssSelector("div#account-login div.alert");
    Wait<WebDriver> wait = new WebDriverWait(driver, 10);

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void login(String email, String password) {
        wait.until(e -> driver.findElement(EMAIL_FIELD));
        type(EMAIL_FIELD, email);
        type(PASSWORD_FIELD, password);
        click(LOGIN_BUTTON);
    }

    public boolean isLoginFailed() {
        return isDisplayed(UNSUCCESSFUL_LOGIN_ALERT);
    }
}
