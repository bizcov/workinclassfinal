package pragmatic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pragmatic.pages.base.BasePage;

public class HomePage extends BasePage {

    public static final By MY_ACCOUNT = By.cssSelector("a[title='My Account']");
    private static final By REGISTER = By.xpath("//a[text()='Register']");
    private static final By LOGIN = By.xpath("//a[text()='Login']");
    Wait<WebDriver> wait = new WebDriverWait(driver, 10);

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void gotoRegister() {
        wait.until(e -> driver.findElement(MY_ACCOUNT)).click();
        click(REGISTER);
    }

    public void gotoLogin() {
        wait.until(e -> driver.findElement(MY_ACCOUNT)).click();
        click(LOGIN);
    }
}
