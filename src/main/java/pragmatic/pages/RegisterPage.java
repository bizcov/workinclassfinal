package pragmatic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pragmatic.pages.base.BasePage;
import static org.testng.Assert.*;

public class RegisterPage extends BasePage {

    private static final By ACCOUNT = By.id("content");
    private static final By FIRST_NAME = By.id("input-firstname");
    private static final By LAST_NAME = By.id("input-lastname");
    private static final By EMAIL = By.id("input-email");
    private static final By TELEPHONE = By.id("input-telephone");
    private static final By PASSWORD = By.id("input-password");
    private static final By PASSWORD_CONFIRM = By.id("input-confirm");
    private static final By SUBSCRIBE = By.name("newsletter");
    private static final By AGREE_POLICY = By.name("agree");
    private static final By CONTINUE_BUTTON = By.xpath("//input[@value='Continue']");
    private static final By WRONG_FIRST_NAME = By.xpath("//input[@id='input-firstname']/following-sibling::div[@class='text-danger']");
    private static final By WRONG_LAST_NAME = By.xpath("//input[@id='input-lastname']/following-sibling::div[@class='text-danger']");
    private static final By WRONG_EMAIL = By.xpath("//input[@id='input-email']/following-sibling::div[@class='text-danger']");
    private static final By WRONG_TELEPHONE = By.xpath("//input[@id='input-telephone']/following-sibling::div[@class='text-danger']");
    private static final By WRONG_PASSWORD = By.xpath("//input[@id='input-password']/following-sibling::div[@class='text-danger']");
    private static final By WRONG_PASSWORD_CONFIRM = By.xpath("//input[@id='input-confirm']/following-sibling::div[@class='text-danger']");
    Wait<WebDriver> wait = new WebDriverWait(driver, 10);

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public boolean isAccountDisplayed() {
        wait.until(e -> driver.findElement(ACCOUNT));
        return isDisplayed(ACCOUNT);
    }

    public void populateRegisterFieldsAndRegister(String firstName, String lastName, String email, String telephone, String password,String confirmPassword) {
        type(FIRST_NAME, firstName);
        type(LAST_NAME, lastName);
        type(EMAIL, email);
        type(TELEPHONE, telephone);
        type(PASSWORD, password);
        type(PASSWORD_CONFIRM, confirmPassword);
        click(SUBSCRIBE);
        assertTrue(driver.findElement(SUBSCRIBE).isSelected());
        click(AGREE_POLICY);
        assertTrue(driver.findElement(AGREE_POLICY).isSelected());
        click(CONTINUE_BUTTON);
    }

    public boolean isAlertDisplayed() {
        boolean isDisplayed = true;
        wait.until(e -> driver.findElement(WRONG_FIRST_NAME));
        if (!isDisplayed(WRONG_FIRST_NAME) || !isDisplayed(WRONG_LAST_NAME) || !isDisplayed(WRONG_EMAIL) || !isDisplayed(WRONG_TELEPHONE) || !isDisplayed(WRONG_PASSWORD) || !isDisplayed(WRONG_PASSWORD_CONFIRM)) {
            isDisplayed = false;
        }
        return isDisplayed;
    }
}
