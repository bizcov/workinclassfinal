package pragmatic.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pragmatic.pages.base.BasePage;

public class SuccessfulLoginPage extends BasePage {

    private static final By LOGOUT = By.xpath("//a[text()='Logout']");
    Wait<WebDriver> wait = new WebDriverWait(driver, 10);

    public SuccessfulLoginPage(WebDriver driver) {
        super(driver);
    }

    public boolean confirmLogin() {
        wait.until(e -> driver.findElement(HomePage.MY_ACCOUNT));
        click(HomePage.MY_ACCOUNT);
     return isDisplayed(LOGOUT);
    }
}
