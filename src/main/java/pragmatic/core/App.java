package pragmatic.core;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pragmatic.pages.*;

public class App {

    private WebDriver driver;
    public HomePage homepage;
    public RegisterPage registerPage;
    public SuccessfulLoginPage successfulLoginPage;
    public LoginPage loginPage;

    public void startBrowser(String pageToOpen) {

        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();

        homepage = new HomePage(driver);
        registerPage = new RegisterPage(driver);
        successfulLoginPage = new SuccessfulLoginPage(driver);
        loginPage = new LoginPage(driver);

        driver.get(pageToOpen);
    }

    public void quit() {
        driver.quit();
    }
}
